package by.training.task;


import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class NodePost extends AbstractWebScript {

    private static final String EXCEPTION_EMPTY_CONTENT = "Empty content";
    private static final String EXCEPTION_EMPTY_JSON_BODE = "Exception empty json bode";
    private static final String EXCEPTION_NODE_NOT_FOUND = "Node not found";
    private static final String EXCEPTION_INVALID_NODE_REFERENCE_FORMAT = "Invalid node reference format";
    private static final String EXCEPTION_PARENT_NODE_NOT_FOUND = "Parent node not found";

    private static final String JSON_KEY_PARENT_NODE_REF = "parentNodeRef";
    private static final String JSON_KEY_NODE_REF = "nodeRef";
    private static final String JSON_KEY_NODE_NAME = "nodeName";
    private static final String JSON_KEY_NODE_TYPE = "type";
    private static final String JSON_KEY_ASPECTS = "aspects";
    private static final String JSON_KEY_PROPERTY_NAME = "name";
    private static final String JSON_KEY_PROPERTY_VALUE = "value";
    private static final String JSON_KEY_PROPERTIES = "properties";

    private static final String ERROR_JSON = "JSON Error";

    private static final String ASSOCIATION_CONTAINS = "cm:contains";

    static final Logger logger = LogManager.getLogger(NodePost.class);

    private ServiceRegistry serviceRegistry;

    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {

        JSONObject jsonContent = (JSONObject) webScriptRequest.parseContent();
        if (jsonContent == null) {
            logger.error(EXCEPTION_EMPTY_CONTENT);
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, EXCEPTION_EMPTY_CONTENT);
        }
        try {
            String parentNodeRefStr = jsonContent.getString(JSON_KEY_PARENT_NODE_REF);
            String nodeName = jsonContent.getString(JSON_KEY_NODE_NAME);
            JSONObject typeObject = jsonContent.getJSONObject(JSON_KEY_NODE_TYPE);
            String typeName = typeObject.getString(JSON_KEY_NODE_NAME);
            JSONArray typeProperties = typeObject.getJSONArray(JSON_KEY_PROPERTIES);
            JSONArray aspectsArray = jsonContent.getJSONArray(JSON_KEY_ASPECTS);

            if (NodeRef.isNodeRef(parentNodeRefStr)) {
                NodeRef parentNodeRef = new NodeRef(parentNodeRefStr);
                NodeService nodeService = serviceRegistry.getNodeService();
                NamespaceService namespaceService = serviceRegistry.getNamespaceService();

                if (nodeService.exists(parentNodeRef)) {
                    QName containsAssocType = ContentModel.ASSOC_CONTAINS;
                    QName assocName = QName.createQName(containsAssocType.getNamespaceURI(), nodeName);
                    QName nodeType = QName.createQName(typeName, namespaceService);


                    Map<QName, Serializable> typePropertiesMap = new HashMap<>();
                    for (int i = 0; i < typeProperties.length(); i++) {
                        JSONObject property = typeProperties.getJSONObject(i);
                        QName propertyName = QName.createQName(property.getString(JSON_KEY_PROPERTY_NAME), namespaceService);
                        Serializable propertyValue = (Serializable) property.get(JSON_KEY_PROPERTY_VALUE);
                        typePropertiesMap.put(propertyName, propertyValue);
                    }
                    typePropertiesMap.put(QName.createQName(ASSOCIATION_CONTAINS, namespaceService), typeName);


                    NodeRef childNodeRef = nodeService.getChildByName(parentNodeRef, containsAssocType, nodeName);
                    if (childNodeRef == null) {

                        ChildAssociationRef childAssociationRef = nodeService
                                .createNode(parentNodeRef, containsAssocType, assocName, nodeType, typePropertiesMap);
                        childNodeRef = childAssociationRef.getChildRef();
                    } else {
                        nodeService.setType(childNodeRef, nodeType);
                        nodeService.setProperties(childNodeRef, typePropertiesMap);
                    }

                    for (int i = 0; i < aspectsArray.length(); i++) {
                        JSONObject aspectObject = aspectsArray.getJSONObject(i);

                        QName aspectName = QName.createQName(aspectObject.getString(JSON_KEY_PROPERTY_NAME), namespaceService);

                        JSONArray aspectProperties = aspectObject.getJSONArray(JSON_KEY_PROPERTIES);

                        nodeService.addAspect(childNodeRef, aspectName, mapCreator(aspectProperties, namespaceService));
                    }
                    webScriptResponse.setStatus(Status.STATUS_CREATED);
                } else {
                    logger.error(EXCEPTION_PARENT_NODE_NOT_FOUND);
                    throw new WebScriptException(Status.STATUS_NOT_FOUND, EXCEPTION_PARENT_NODE_NOT_FOUND);
                }

            } else {
                logger.error(EXCEPTION_INVALID_NODE_REFERENCE_FORMAT);
                throw new WebScriptException(Status.STATUS_BAD_REQUEST, EXCEPTION_INVALID_NODE_REFERENCE_FORMAT);
            }

        } catch (JSONException e) {
            logger.error(ERROR_JSON, e);
        }
    }

    private Map<QName, Serializable> mapCreator(JSONArray aspectProperties, NamespaceService namespaceService) {
        Map<QName, Serializable> aspectPropertiesMap = new HashMap<>();
        for (int j = 0; j < aspectProperties.length(); j++) {
            JSONObject property = null;
            try {
                property = aspectProperties.getJSONObject(j);
                QName propertyName = QName.createQName(property.getString(JSON_KEY_PROPERTY_NAME), namespaceService);
                Serializable propertyValue = (Serializable) property.get(JSON_KEY_PROPERTY_VALUE);
                aspectPropertiesMap.put(propertyName, propertyValue);
            } catch (JSONException e) {
                logger.error(ERROR_JSON, e);
            }
        }
        return aspectPropertiesMap;
    }
}