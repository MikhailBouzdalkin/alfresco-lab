package by.training.task;

import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.*;

import java.io.IOException;

public class NodeDelete extends AbstractWebScript {
    private static final String EXCEPTION_EMPTY_CONTENT = "Empty content";
    private static final String EXCEPTION_EMPTY_JSON_BODE = "Exception empty json bode";
    private static final String EXCEPTION_NODE_NOT_FOUND = "Node not found";
    private static final String EXCEPTION_INVALID_NODE_REFERENCE_FORMAT = "Invalid node reference format";
    private static final String EXCEPTION_PARENT_NODE_NOT_FOUND = "Parent node not found";

    private static final String JSON_KEY_NODE_REF = "nodeRef";

    static final Logger logger = LogManager.getLogger(NodeDelete.class);

    private ServiceRegistry serviceRegistry;

    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        JSONObject jsonContent = (JSONObject) webScriptRequest.parseContent();
        if (jsonContent == null) {
            logger.error(EXCEPTION_EMPTY_CONTENT);
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, EXCEPTION_EMPTY_CONTENT);
        }
        String nodeRefStr = null;
        try {
            nodeRefStr = jsonContent.getString(JSON_KEY_NODE_REF);
        } catch (JSONException e) {
            logger.error(EXCEPTION_EMPTY_JSON_BODE, e);
        }

        if (NodeRef.isNodeRef(nodeRefStr)) {
            NodeRef nodeRef = new NodeRef(nodeRefStr);

            NodeService nodeService = serviceRegistry.getNodeService();

            if (nodeService.exists(nodeRef)) {
                nodeService.deleteNode(nodeRef);

                webScriptResponse.setStatus(Status.STATUS_ACCEPTED);
            } else {
                logger.error(EXCEPTION_NODE_NOT_FOUND);
                throw new WebScriptException(Status.STATUS_NOT_FOUND, EXCEPTION_NODE_NOT_FOUND);
            }
        } else {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, EXCEPTION_INVALID_NODE_REFERENCE_FORMAT);
        }
    }
}