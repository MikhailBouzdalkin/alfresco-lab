package by.training.task;


import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

public class NodeGet extends AbstractWebScript {
    private static final String EXCEPTION_EMPTY_CONTENT = "Empty content";
    private static final String EXCEPTION_EMPTY_JSON_BODE = "Exception empty json bode";
    private static final String EXCEPTION_NODE_NOT_FOUND = "Node not found";
    private static final String EXCEPTION_INVALID_NODE_REFERENCE_FORMAT = "Invalid node reference format";
    private static final String EXCEPTION_PARENT_NODE_NOT_FOUND = "Parent node not found";

    private static final String PARAMETER_KEY_CHILD_NODE="childNode";
    private static final int RESPONSE_STATUS_NOT_FOUNT = 404;
    private static final String MESSAGE_NODE_SERVICE_NOT_FOUND = "NodeService not find.";
    private static final String MESSAGE_NODE_WRONG_NODE_REFERENCE_PARAMETER = "Wrong node reference parameter.";
    private static final String JSON_KEY_NODE_REF = "nodeRef";
    private static final String JSON_KEY_NODE_NAME = "nodeName";
    private static final String JSON_KEY_NODE_TYPE = "type";
    private static final String JSON_KEY_ASPECTS = "aspects";
    private static final String JSON_KEY_PROPERTY_NAME = "name";
    private static final String JSON_KEY_PROPERTY_VALUE = "value";
    private static final String JSON_KEY_PROPERTIES = "properties";

    private static final String RESPONSE_CONTENT_TYPE_JSON = "application/json";
    private static final String RESPONSE_ENCODING_UTF_8 = "utf-8";


    static final Logger logger = LogManager.getLogger(NodeGet.class);

    private ServiceRegistry serviceRegistry;

    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {

        String nodeRefString = webScriptRequest.getParameter(PARAMETER_KEY_CHILD_NODE);

        if (NodeRef.isNodeRef(nodeRefString)) {

            NodeRef nodeRef = new NodeRef(nodeRefString);
            NodeService nodeService = serviceRegistry.getNodeService();
            NamespaceService namespaceService = serviceRegistry.getNamespaceService();

            if (nodeService == null) {
                webScriptResponse.setStatus(RESPONSE_STATUS_NOT_FOUNT);
                webScriptResponse.getWriter().write(MESSAGE_NODE_SERVICE_NOT_FOUND);
            }

            if (nodeService.exists(nodeRef)) {
                JSONObject jsonResponse = new JSONObject();
                try {
                    String[] pathToNodeName = nodeService.getPath(nodeRef).toPrefixString(namespaceService).split("/");
                    jsonResponse.put(JSON_KEY_NODE_REF, nodeRefString);
                    jsonResponse.put(JSON_KEY_NODE_NAME, pathToNodeName[pathToNodeName.length - 1]);
                    String type = nodeService.getType(nodeRef).toPrefixString(namespaceService);
                    jsonResponse.put(JSON_KEY_NODE_TYPE, type);
                    JSONArray aspectsArray = new JSONArray();
                    for (QName aspect : nodeService.getAspects(nodeRef)) {
                        aspectsArray.put(aspect.toPrefixString(namespaceService));
                    }
                    jsonResponse.put(JSON_KEY_ASPECTS, aspectsArray);

                    Map<QName, Serializable> nodeProperties = nodeService.getProperties(nodeRef);
                    JSONArray propertiesArray = new JSONArray();
                    for (Map.Entry<QName, Serializable> propertyEntry : nodeProperties.entrySet()) {
                        JSONObject propertyObject = new JSONObject();
                        propertyObject.put(JSON_KEY_PROPERTY_NAME, propertyEntry.getKey().toPrefixString(namespaceService));
                        propertyObject.put(JSON_KEY_PROPERTY_VALUE, propertyEntry.getValue());

                        propertiesArray.put(propertyObject);
                    }
                    jsonResponse.put(JSON_KEY_PROPERTIES, propertiesArray);

                } catch (JSONException e) {
                    logger.error(EXCEPTION_EMPTY_JSON_BODE, e);
                }

                webScriptResponse.setStatus(Status.STATUS_OK);
                webScriptResponse.setContentType(RESPONSE_CONTENT_TYPE_JSON);
                webScriptResponse.setContentEncoding(RESPONSE_ENCODING_UTF_8);
                webScriptResponse.getWriter().write(jsonResponse.toString());
            } else {
                throw new WebScriptException(Status.STATUS_NOT_FOUND, EXCEPTION_NODE_NOT_FOUND);
            }
        } else {
            throw new WebScriptException(Status.STATUS_BAD_REQUEST, MESSAGE_NODE_WRONG_NODE_REFERENCE_PARAMETER);
        }
    }
}